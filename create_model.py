#  Copyright (c) 2019. v1.0 under a creative commons license specified in the LICENSE.md file included in the repository.

# trains a model on the text file in path and saves to the models folder with an automatically generated name

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from textgenrnn.textgenrnn import textgenrnn
import os

path = '/home/jimmy/PycharmProjects/test1/news_troop_text_generator/datasets/formatted_text/all_bbc_formatted.txt'

# determine number to append to filename
i = True
count = 0
while os.path.exists('/home/jimmy/PycharmProjects/test1/news_troop_text_generator/models/news_troop' + str(count)+'.hdf5'):
    count += 1

# train and save a new model
textgen = textgenrnn()
textgen.train_from_file(path, num_epochs=2)
textgenrnn.save(textgen, '/home/jimmy/PycharmProjects/test1/news_troop_text_generator/models/news_troop'+str(count)+'.hdf5')
textgen.generate()
