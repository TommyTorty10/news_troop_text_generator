# [NEWS TROOP Text Generator v1.0](https://sites.google.com/view/newstroop/home)
<img style="-webkit-user-select: none;margin: auto;" src="https://cdn.discordapp.com/attachments/637125522886885442/637165414539067412/NEWSTROOPTEST-2.gif">

## What is NEWS TROOP
[We are a news outlet like no other.](https://sites.google.com/view/newstroop/home)

[Visit here to read more about us.](https://sites.google.com/view/newstroop/info/about-us)

## Installation
Python 3.7 or greater and the necessary python packages are required to use the rnn.

To install required packages:

**pip install -r requirements.txt**

Necessary packages include:
    tensorflow 2,
    setuptools,
    textgenrnn

If textgenrnn throws the error "ImportError: cannot import name 'multi_gpu_model' from 'tensorflow.keras.utils'"
then you might want to clone its git repo instead of installing with pip.

## Usage
### Creating a New Model
* Go to create_model.py.
* Set **path** variable to the filepath of a **single text file** that you want to train the rnn on.
* Run create_model.py.
* The trained model will be in the models folder with name news_troopx.hdf5.

### Generating Text
* Go text_generator.py.
* Change **path** to the filepath of the trained model, that you want to use.
* Set **length** variable to the length of string that you would like to be generated.


## Copyright and License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">NEWS TROOP Text Generator</span> 
by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/TommyTorty10" property="cc:attributionName" rel="cc:attributionURL">James Kolby</a> is licensed under a 
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://github.com/minimaxir/textgenrnn" rel="dct:source">https://github.com/minimaxir/textgenrnn</a>.
