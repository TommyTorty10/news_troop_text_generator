#  Copyright (c) 2019. v1.0 under a creative commons license specified in the LICENSE.md file included in the repository.

# will generate text with model at path.  change length to change length of text

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from textgenrnn.textgenrnn import textgenrnn

path = 'models/news_troop3.hdf5'
length = 600

textgen = textgenrnn(path)
text = textgen.generate(max_gen_length=length, return_as_list=True)[0]
print(text)
